/**
 * @File Name          : SP_StudentApplications_Trigger.trigger
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 07-14-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   dmorales     Initial Version
**/
trigger SP_StudentApplications_Trigger on Application__c (before delete) {
    SP_TriggerSwitch__c spts = SP_TriggerSwitch__c.getOrgDefaults();
    //If trigger custom setting is active
    if(spts.SP_ApplicationTrigger__c || Test.isRunningTest()) {
        SP_StudentApplicationTrigger_Utility.handleRelatedDocument(Trigger.OldMap.keySet());    
    }           
    
}