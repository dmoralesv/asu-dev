/**
 * @File Name          : SP_ContentDocumentLinkTrigger_Utility.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   dmorales     Initial Version
**/
public without sharing class SP_ContentDocumentLinkTrigger_Utility {
    public static void handleContentDocumentSync(List<ContentDocumentLink> cdlList) {

        Map<Id,Id> entityContentMap = new Map<Id,Id>();
        for(ContentDocumentLink cdLinkObj : cdlList){
            entityContentMap.put(cdLinkObj.LinkedEntityId, cdLinkObj.ContentDocumentId);
        }

        Map<ID, Application__c> appMap = new Map<ID, Application__c>([SELECT Id, Application_Status__c, Sync_Status__c
                                                                     FROM Application__c
                                                                     WHERE Id IN :entityContentMap.keySet()]);    
        
                                                                        
                    
        List<Id> contentDocumentIdsList = new List<Id>();                                                                     
        for(Id appObjId : appMap.keySet()){
            Application__c app = appMap.get(appObjId);
            if(app.Application_Status__c == 'Accepted'){          
                contentDocumentIdsList.add(entityContentMap.get(appObjId));
            }
        } 
        
        List<ContentVersion> cvList = [SELECT Id, IsReadyToSync__c, Sent__c, ContentDocumentId 
                                       FROM ContentVersion 
                                       WHERE ContentDocumentId IN: contentDocumentIdsList];

        for(ContentVersion cvObj : cvList){
           cvObj.IsReadyToSync__c = TRUE; 
           cvObj.Sent__c = FALSE; 
        }
          
        update cvList;  
    
     
    }
}