/**
 * @File Name          : SP_TestDataFactory.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 09-14-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   dmorales     Initial Version
**/
@isTest
public class SP_TestDataFactory {
    public static void createApplicationWithDocument(String appStatus) {
        Application__c app = new Application__c( Application_Form_Type__c = 'Full Time',
                                                 Student_First_Name__c = 'TestingStudentFirstName',
                                                 Student_Last_Name__c = 'TestingStudentLastName',
                                                 Application_Status__c = appStatus );
        insert app;

        ContentVersion contentVersion = new ContentVersion(
        Title = 'TestDocumentApplication',
        PathOnClient = 'testDocumentApplication.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true
        );

        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = app.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;      
    }

    public static void createApplicationWithDocument(String appStatus, String syncStatus) {
        Application__c app = new Application__c( Application_Form_Type__c = 'Full Time',
                                                 Student_First_Name__c = 'TestingStudentFirstName',
                                                 Student_Last_Name__c = 'TestingStudentLastName',
                                                 Application_Status__c = appStatus,
                                                 Sync_Status__c  = syncStatus );
        insert app;

        ContentVersion contentVersion = new ContentVersion(
        Title = 'TestDocumentApplication',
        PathOnClient = 'testDocumentApplication.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true
        );

        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = app.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;      
    }
}