/**
 * @File Name          : SP_UpdateRelatedReadySync.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10-15-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/3/2020   dmorales     Initial Version
 **/
public without sharing class SP_UpdateRelatedReadySync {
    
    @InvocableMethod(label='Set Content Ready To Sync' 
    description='Identify if there are any File related to the applications and setting up them for sync')
    public static void handleSyncProcessReady(List<Application__c> appList) {
        SP_ApplicationsSyncManagement.handleUpdateRelatedContent(appList, false, false, true);
    }
}