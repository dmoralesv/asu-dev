/**
 * @File Name          : SP_ApplicationsSyncManagement.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10-15-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/29/2020   dmorales     Initial Version
 **/
public without sharing class SP_ApplicationsSyncManagement {


	public static void handleUpdateRelatedContent(List<Application__c> appList, 
												Boolean updateSent,
												Boolean isSent,  
												Boolean isReadyToSync) {

		Set<Id> appIdsList = new Map<Id, Application__c>(appList).keySet();

		List<ContentDocumentLink> contDocLinkList = [SELECT ContentDocumentId, Id
													FROM ContentDocumentLink
													WHERE LinkedEntityId IN :appIdsList];


		List<String> contDocIds = new List<String>();
		for(ContentDocumentLink cdL : contDocLinkList) {
			contDocIds.add(cdL.ContentDocumentId);
		}

		List<ContentVersion> contVersionList = [SELECT Id, Sent__c, IsReadyToSync__c, Title
												FROM ContentVersion
												WHERE ContentDocumentId IN:contDocIds AND IsLatest = TRUE];

		for(ContentVersion cv : contVersionList) {
			cv.IsReadyToSync__c = isReadyToSync;
			if(updateSent)
			  cv.Sent__c = isSent;
		}

		update contVersionList;
	}
}