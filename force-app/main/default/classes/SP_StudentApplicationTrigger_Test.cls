/**
 * @File Name          : SP_StudentApplicationTrigger_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 6/11/2020, 3:38:35 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   dmorales     Initial Version
**/
@IsTest
private class SP_StudentApplicationTrigger_Test {
    @TestSetup
    static void makeData(){
       SP_TestDataFactory.createApplicationWithDocument('Accepted');
    }
    
    @isTest
    static void test() {
     
        List<ContentVersion> cvListBefore = [SELECT IsReadyToSync__c, Sent__c 
                                        FROM ContentVersion 
                                        WHERE Title = 'TestDocumentApplication'
                                        AND PathOnClient = 'testDocumentApplication.jpg'];

        System.assert(cvListBefore[0].IsReadyToSync__c);                                
        System.assert(!cvListBefore[0].Sent__c);   

        List<Application__c> appList = [SELECT Id, Student_First_Name__c FROM Application__c
                                        WHERE Student_First_Name__c = 'TestingStudentFirstName'
                                        AND Student_Last_Name__c = 'TestingStudentLastName'];
       
        Test.startTest();
           delete appList;
        Test.stopTest();
        
        List<ContentVersion> cvListAfter = [SELECT IsReadyToSync__c, Sent__c 
                                            FROM ContentVersion 
                                            WHERE Title = 'TestDocumentApplication'
                                            AND PathOnClient = 'testDocumentApplication.jpg'];

        System.assert(!cvListAfter[0].IsReadyToSync__c);                                
        System.assert(!cvListAfter[0].Sent__c);                                               
        
    }
}