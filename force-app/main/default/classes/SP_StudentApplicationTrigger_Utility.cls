/**
 * @File Name          : SP_StudentApplicationTrigger_Utillity.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 6/11/2020, 3:08:01 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   dmorales     Initial Version
**/
public without sharing class SP_StudentApplicationTrigger_Utility {
    
    public static void handleRelatedDocument(Set<Id> appList) {
      
        List<ContentDocumentLink> cdLinkList = [SELECT ContentDocumentId, Id 
                                                 FROM ContentDocumentLink
                                                 WHERE LinkedEntityId IN: appList];

                                                     

        List<Id> contentDocumentIds = new List<Id>();
        for(ContentDocumentLink cdlObj : cdLinkList){
             contentDocumentIds.add(cdlObj.ContentDocumentId);
        }                                                 

        List<ContentVersion> cvList = [SELECT Id, IsReadyToSync__c, Sent__c, ContentDocumentId 
                                       FROM ContentVersion 
                                       WHERE ContentDocumentId IN: contentDocumentIds];
        
           
        for(ContentVersion cvObj : cvList){
           cvObj.IsReadyToSync__c = FALSE;                
           cvObj.Sent__c = FALSE;                
        }

        update cvList;
    }
}